package com.yuyifei.classify;

import java.util.ArrayList;

public class ClassifyCommon 
{
	//store apk info
	public static ArrayList <ApkInformation> apkList = new ArrayList <ApkInformation>();
	
	private static int number = 5;
	private static String rootDir = "/home/zbh/Desktop/apks";
	
	//markket url
	private static String HuaWeiListUrl = "http://appstore.huawei.com/soft/list";
	private static String HuaWeiMarketName = "HuaWeiMarket";
	private static String JiFengListUrl = "http://apk.gfan.com/apps_1_1_1.html";
	private static String JiFengMarketName = "JiFeng";
	private static String BaiDuListrl = "http://shouji.baidu.com/software";
	private static String BaiDuMarketName = "BaiDu";

	/*
	 * set and get methods
	 */
	public static int getNumber() {
		return number;
	}

	public static String getBaiDuListrl() {
		return BaiDuListrl;
	}

	public static void setBaiDuListrl(String baiDuListrl) {
		BaiDuListrl = baiDuListrl;
	}

	public static String getBaiDuMarketName() {
		return BaiDuMarketName;
	}

	public static void setBaiDuMarketName(String baiDuMarketName) {
		BaiDuMarketName = baiDuMarketName;
	}

	public static String getHuaWeiMarketName() {
		return HuaWeiMarketName;
	}

	public static void setHuaWeiMarketName(String huaWeiMarketName) {
		HuaWeiMarketName = huaWeiMarketName;
	}

	public static String getRootDir() {
		return rootDir;
	}

	public static void setRootDir(String rootDir) {
		ClassifyCommon.rootDir = rootDir;
	}

	public static void setNumber(int number) {
		ClassifyCommon.number = number;
	}
	
	public static String getHuaWeiListUrl() {
		return HuaWeiListUrl;
	}
	
	public static void setHuaWeiListUrl(String huaWeiListUrl) {
		HuaWeiListUrl = huaWeiListUrl;
	}
	
	public static String getJiFengListUrl() {
		return JiFengListUrl;
	}

	public static void setJiFengListUrl(String jiFengListUrl) {
		JiFengListUrl = jiFengListUrl;
	}

	public static String getJiFengMarketName() {
		return JiFengMarketName;
	}

	public static void setJiFengMarketName(String jiFengMarketName) {
		JiFengMarketName = jiFengMarketName;
	}
	
}
