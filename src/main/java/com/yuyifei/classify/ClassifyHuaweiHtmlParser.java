package com.yuyifei.classify;

import java.io.File;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.yuyifei.common.Html;
import com.yuyifei.util.Debug;
import com.yuyifei.util.DownloadOperation;
import com.yuyifei.util.FileOperation;

public class ClassifyHuaweiHtmlParser extends ClassifyParser
{

	@Override
	public void parserHtml() 
	{
		HashMap <String, String> hm = new HashMap <String, String>();
		String url = ClassifyCommon.getHuaWeiListUrl();
	
		//System.out.println(url);
		String html = Html.getHtmlFromUrl(url);
		//System.out.println(html);
		
		Document doc = Jsoup.parse(html);
		Elements as = doc.select("span.head-right > a");
		System.out.println("as_size:" + as.size());
		
		for (Element a : as)
		{
			String text = a.text();//classify name
			String attr = a.attr("href");//new url
			
			//System.out.println(text + ":" + attr);
			
			hm.put(text, attr);
		}
		
		for (String key : hm.keySet())
		{
			//System.out.println("key:" + key);	
			downloadApk(key, hm.get(key));
		}
	}

	private void downloadApk(String key, String url)
	{
		int count = 0;
		String html = Html.getHtmlFromUrl(url);
		
		Document doc = Jsoup.parse(html);
		Element unitDiv = doc.select("div.unit-main").first();
		Elements divs = unitDiv.select("div.list-game-app");
		for (Element div : divs)
		{
			//System.out.println(div.select("a.btn-blue").size());
			Element a = div.select("a.btn-blue").first();
			//System.out.println(a.attr("onclick"));
			String[] str = a.attr("onclick").split("'");
			//Debug.printStringArray(str);
			
			//some attr about apk file
			String apkName = str[3] + ".apk";
			String classifyName = str[9];
			String downloadUrl = str[11];
			
			//creat dir
			String storeDir = ClassifyCommon.getRootDir() + "/" 
					+ ClassifyCommon.getHuaWeiMarketName() + "/"
					+ classifyName;
			FileOperation.creatDir(storeDir);//create dir
			
			File apkFile = DownloadOperation.downloadFromUrl(storeDir, apkName, downloadUrl);
			
			//store apk info
			ApkInformation apk = new ApkInformation();
			apk.setApkMarketName(ClassifyCommon.getHuaWeiMarketName());
			apk.setApkName(apkName);
			apk.setDownloadUrl(downloadUrl);
			apk.setStorePath(apkFile.getAbsolutePath());
			ClassifyCommon.apkList.add(apk);
			
			++count;//count download number
			if (count >= ClassifyCommon.getNumber())
			{
				System.out.println("download apk file number: " + count);
				break;
			}
		}
	}

}
