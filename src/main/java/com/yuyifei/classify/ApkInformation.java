package com.yuyifei.classify;

public class ApkInformation
{
	//apk file info
	private String apkMarketName = "";
	private String apkName = "";
	private String downloadUrl = "";
	private String storePath = "";
	
	//set and get methods
	public String getApkMarketName() {
		return apkMarketName;
	}
	public void setApkMarketName(String apkMarketName) {
		this.apkMarketName = apkMarketName;
	}
	public String getApkName() {
		return apkName;
	}
	public void setApkName(String apkName) {
		this.apkName = apkName;
	}
	public String getDownloadUrl() {
		return downloadUrl;
	}
	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}
	public String getStorePath() {
		return storePath;
	}
	public void setStorePath(String storePath) {
		this.storePath = storePath;
	}
}
